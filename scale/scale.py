'''
Created on Nov 28, 2014

@author: slavko
'''
import time
import datetime
import logging

PAIRING_TIMEOUT = 60
AWAKE_TIMEOUT = 20
MEASURE_TIMEOUT = 5
WEIGHT_WAKE_UP = 10
CONNECT_TIMEOUT = 60
STEPON_TIMEOUT = 20
MEASUREMENT_STORAGE_LIMIT = 10

logger = logging.getLogger(__name__).addHandler(logging.NullHandler())

class WeightMeasurement(object):
    def __init__(self, weight):
      self.weight = weight
      self.time = datetime.datetime.now()
      
    def __str__(self, *args, **kwargs):
      return ("Weight = %d, time = " % self.weight ) + self.time.strftime("%y-%m-%d,%H:%M:%S")
      
class ScaleMockup(object):
    def __init__(self, app):
      self.app = app
      self.bonding_mode = True
      self.notifications = [
                       "0",
                       "C0D",
                       "0",
                       "CC00DD",
                       "CCCCDDD",
                       "DDDDDDD",
                       ]
      self.restart()
        
    def restart(self):
      self.measurements = []
      self.current_measurement = WeightMeasurement( 0 )
      self.current_state = 'power_off'
      self.current_notification = 0
      self.step_on = False
      self.batteries = False
      self.pairing_button = False
      self.paired_phone = None
      self.bonded_phone = None
      self.pairing_timeout = 0
      self.measure_timeout = 0
      self.connect_timeout = 0
      self.current_notification_index = 0
      self.step_on_flag = False
      self.step_on_flag_ack = False
      self.sound = '0'
      logger.info("BONDING MODE = %d" % self.bonding_mode)
      
    def process_notification(self):
      if self.current_notification:
        if self.current_notification_index < len(self.notifications[self.current_notification]):
          self.sound = self.notifications[self.current_notification][self.current_notification_index]
          self.current_notification_index = self.current_notification_index + 1
        else:
          self.current_notification_index = 0
          self.current_notification = 0
          self.sound = '0'
           
    def enter_state_power_off(self):
      self.pairing_timeout = 0
      self.measure_timeout = 0
      self.connect_timeout = 0
      self.current_notification = 0

    def state_power_off(self):
      if self.batteries:
        return 'power_on'
      return 'power_off'
    
    def enter_state_power_on(self):
      self.current_notification = 1

    def state_power_on(self):
      if self.bonding_mode:
        return 'pairing_and_bonding_sleep'
      else:
        return 'sleep'
      
    def exit_state_power_on(self):
      self.measurements = []
      self.paired_phone = None
      self.bonded_phone = None
      self.pairing_timeout = 0
      self.measure_timeout = 0
      self.connect_timeout = 0
      self.current_measurement = WeightMeasurement( 0 )

    def state_pairing_and_bonding_sleep(self):
      if self.pairing_button:
        return 'pairing_and_bonding'
      return 'pairing_and_bonding_sleep'
    
    def enter_state_pairing_and_bonding(self):
      self.pairing_timeout = time.time()
            
    def state_pairing_and_bonding(self):
      if not self.batteries:
        return 'power_off'
      self.app.advertize_pairing(self, True)
      if time.time() - self.pairing_timeout > PAIRING_TIMEOUT:
        return 'bonding_failed'
      if self.bonded_phone:
        return 'bonding_success'
      return 'pairing_and_bonding'

    def exit_state_pairing_and_bonding(self):
      self.pairing_timeout = 0
      self.app.advertize_pairing(self, False)
      
    def enter_state_bonding_failed(self):
      self.current_notification = 5
      
    def state_bonding_failed(self):
      return 'pairing_and_bonding_sleep'
    
    def enter_state_bonding_success(self):
      self.current_notification = 1
      
    def state_bonding_success(self):
      return 'sleep'
    
    def state_sleep(self):
      if not self.batteries:
        return 'power_off'
      if self.step_on_flag:
        return 'step_on'
      return 'sleep'

    def enter_state_step_on(self):    
      self.step_on_flag_ack = True
      self.measure_timeout = time.time()
      self.current_notification = 1

    def state_step_on(self):
      if not self.batteries:
        return 'power_off'
      if self.step_on and self.current_measurement.weight > WEIGHT_WAKE_UP:
        if time.time() - self.measure_timeout > MEASURE_TIMEOUT:
          return 'store'
      else:
        return 'bt_sync'
      return 'step_on'
      
    def exit_state_step_on(self):    
      self.measure_timeout = 0

    def state_store(self):
      if not self.batteries:
        return 'power_off'
      if self.current_measurement.weight:
        if len( self.measurements ) < MEASUREMENT_STORAGE_LIMIT:
          self.measurements.append(self.current_measurement)
          return 'store_success'
        else:
          return 'store_failed'
      return 'bt_sync'
    
    def enter_state_store_success(self):
      self.current_notification = 3
      
    def state_store_success(self):
      return 'bt_sync'
    
    def state_store_failed(self):
      return 'bt_sync'
    
    def enter_state_bt_sync(self):
      self.connect_timeout = time.time()

    def state_bt_sync(self):
      if not self.batteries:
        return 'power_off'
      if time.time() - self.connect_timeout <= CONNECT_TIMEOUT:
        if self.step_on_flag:
          return 'step_on'
        else:
          self.app.pair(self)
          if self.app.connect(self, True):
            if len(self.measurements):
              success_index = 0
              for measurement in self.measurements:
                if self.app.send_measurement( measurement ):
                  success_index = success_index + 1
                else:
                  break              
              del self.measurements[0:success_index]
              return 'bt_sync_success'
        return 'bt_sync'
      else:
        if len(self.measurements):
          return 'bt_sync_failed'
        else:
          return 'sleep'
      
    def exit_state_bt_sync(self):
      self.connect_timeout = 0
      self.app.connect(self, False)     
        
    def enter_state_bt_sync_success(self):
      self.current_notification = 4
      
    def state_bt_sync_success(self):
      return 'bt_sync'
        
    def enter_state_bt_sync_failed(self):
      self.current_notification = 5
      
    def state_bt_sync_failed(self):
      return 'sleep'
        
    def process_step_on(self):
      if self.step_on_flag_ack:
        self.step_on_flag = False
      else:
        if self.step_on and self.current_measurement.weight > WEIGHT_WAKE_UP:
          if not self.step_on_flag:
            self.step_on_flag = True
      if not self.step_on:
        self.step_on_flag_ack = False
      
    def run(self):
      try:
        self.process_step_on()
        prev_state = self.current_state
        new_state = getattr(self, 'state_' + self.current_state)()
        if new_state != prev_state:
          try:
            state_exit = getattr(self, 'exit_state_' + prev_state)
            logger.debug('exit_state_' + prev_state)
            state_exit()
          except AttributeError:
            pass
          try:
            state_enter = getattr(self, 'enter_state_' + new_state)
            logger.debug('enter_state_' + new_state)
            state_enter()
          except AttributeError:
            pass
          self.current_state = new_state
          logger.debug(self.current_state)
        self.process_notification()
      except:
        logger.exception("")
