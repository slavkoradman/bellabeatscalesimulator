'''
Created on Nov 28, 2014

@author: slavko
'''
import logging
logger = logging.getLogger(__name__).addHandler(logging.NullHandler())

class ScaleApp(object):
    def __init__(self):
      self.bonding_mode = True
      self.bt_enabled = False
      self.restart()
    
    def restart(self):
      self.paired_scale = None
      self.bonded_scale = None
      self.measurements = []
      self.advertised_scale = None
      self.connected = False
      logger.info("BONDING MODE = %d" % self.bonding_mode)
      
    def bt_enable(self, value):
      self.bt_enabled = value
      self.paired_scale = None
      self.advertised_scale = None
      self.connected = False
        
    def advertize_pairing(self, advertised_scale, value):
      if not value:
        self.advertised_scale = None
      else:
        if self.bt_enabled and not self.bonded_scale:
          self.advertised_scale = advertised_scale
        
    def pair(self, scale):
      if self.bonding_mode:
        if self.bt_enabled and self.bonded_scale == scale:
          self.paired_scale = scale
          self.paired_scale.paired_phone = self
      else:
        if self.bt_enabled:
          self.paired_scale = scale
          self.paired_scale.paired_phone = self

    def bond(self, value):
      if self.bonding_mode:
        if not value:
          if self.bonded_scale:
            self.bonded_scale.bonded_phone = None
          self.paired_scale = None
          self.bonded_scale = None
          self.connected = False
          self.advertised_scale = None
          return        
        if self.bt_enabled and self.advertised_scale:
          self.bonded_scale = self.advertised_scale
          self.bonded_scale.bonded_phone = self
          self.advertised_scale = None
        
    def connect(self, scale, value):
      if not value:
        self.connected = False
        self.paired_scale = None
      else:
        if self.bt_enabled and self.paired_scale == scale:
          self.connected = True
      return self.connected
        
    def send_measurement(self, measurement):
      if self.connected:
        self.measurements.append(measurement)
        return True
      return False