'''
Created on Sep 29, 2014

@author: slavko
'''
from Tkinter import *
import tkMessageBox
import logging
import logging.handlers
import scale.scale
import scale.scaleapp
import time

logger = logging.getLogger( 'BellabeatScaleSimulator' )
formatter = logging.Formatter( '[%(asctime)s][%(levelname)s][%(name)s] %(message)s' )
fileHandler = logging.handlers.TimedRotatingFileHandler( 'scale_simulator.log', 'midnight', 1, 14 )
fileHandler.setFormatter( formatter )
conHandler = logging.StreamHandler()
conHandler.setFormatter( formatter )
logger.addHandler( conHandler ) 
logger.addHandler( fileHandler ) 
logger.setLevel( logging.DEBUG )   
scale.scale.logger = logging.getLogger("BellabeatScaleSimulator.Scale")
scale.scaleapp.logger = logging.getLogger("BellabeatScaleSimulator.ScaleApp")

PADX=4

class App:
  def __init__(self, master, scale, scaleapp):
    self.master = master
    self.scale = scale
    self.scaleapp = scaleapp
    frame = Frame(master)
    frame.grid( padx=10, pady=10)

    self.mode_frame = LabelFrame(frame, text = " OPERATION MODE ", bg="#a01010")
    self.mode_frame.grid( row = 1, column = 1, padx=PADX, pady=PADX, columnspan=2)

    self.mode_var = IntVar()
    self.mode1 = Radiobutton(self.mode_frame, text="MODE 1 (BONDING)", variable=self.mode_var, value=1, anchor=W)
    self.mode1.grid( row = 1, column =1, padx=PADX, pady=PADX)
    self.mode2 = Radiobutton(self.mode_frame, text="MODE 2", variable=self.mode_var, value=0, anchor=W)
    self.mode2.grid( row = 1, column =2, padx=PADX, pady=PADX)
    self.mode_var.set(1)
    
    self.scale_frame = LabelFrame(frame, text = " SCALE ", bg = "#808010")
    self.scale_frame.grid( row = 2, column = 1, padx=PADX, pady=PADX,)

    self.app_frame = LabelFrame(frame, text = " APP ", bg = "#108080")
    self.app_frame.grid( row = 2, column = 2, padx=PADX, pady=PADX)

    self.scale_actions_frame = LabelFrame(self.scale_frame, text = "Scale actions")
    self.scale_actions_frame.grid( row = 1, column = 1, padx=PADX, pady=PADX)
 
    self.weight_var = IntVar()
    self.weight_scale = Scale(self.scale_actions_frame, variable=self.weight_var, label="Weight", orient=HORIZONTAL, length=400, to=120)
    self.weight_scale.grid( row=1, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.battery_var = IntVar()
    self.battery_checkbutton = Checkbutton(self.scale_actions_frame, text="BATTERY INSERTED", width=30, height = 1, command = self.battery_cmd, variable = self.battery_var)
    self.battery_checkbutton.grid(row=2, column = 1, padx=PADX, pady=PADX)
    
    self.pairing_var = IntVar()
    self.pairing_checkbutton = Checkbutton(self.scale_actions_frame, text="PAIRING BUTTON", width=30, height = 1, command = self.pairing_cmd, variable = self.pairing_var)
    self.pairing_checkbutton.grid(row=3, column = 1, padx=PADX, pady=PADX)
    
    self.step_var = IntVar()
    self.step_checkbutton = Checkbutton(self.scale_actions_frame, text="STEP ON", width=30, height = 1, command = self.step_cmd, variable = self.step_var)
    self.step_checkbutton.grid(row=4, column = 1, padx=PADX, pady=PADX)

    self.scale_status_frame = LabelFrame(self.scale_frame, text = "Scale status")
    self.scale_status_frame.grid( row = 2, column = 1, padx=PADX, pady=PADX)
 
    self.scale_status_var = StringVar()
    self.scale_status_label = Label(self.scale_status_frame, textvariable=self.scale_status_var)
    self.scale_status_label.grid( row=1, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.scale_notification_var = StringVar()
    self.scale_notification_label = Label(self.scale_status_frame, textvariable=self.scale_notification_var)
    self.scale_notification_label.grid( row=2, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.scale_sound_var = StringVar()
    self.scale_sound_label = Label(self.scale_status_frame, textvariable=self.scale_sound_var)
    self.scale_sound_label.grid( row=3, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.scale_measurements_var = StringVar()
    self.scale_measurements_label = Label(self.scale_status_frame, textvariable=self.scale_measurements_var)
    self.scale_measurements_label.grid( row=4, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.scale_current_measurement_var = StringVar()
    self.scale_current_measurement_label = Label(self.scale_status_frame, textvariable=self.scale_current_measurement_var)
    self.scale_current_measurement_label.grid( row=5, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.scale_pairing_timeout_var = StringVar()
    self.scale_pairing_timeout_label = Label(self.scale_status_frame, textvariable=self.scale_pairing_timeout_var)
    self.scale_pairing_timeout_label.grid( row=6, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.scale_measure_timeout_var = StringVar()
    self.scale_measure_timeout_label = Label(self.scale_status_frame, textvariable=self.scale_measure_timeout_var)
    self.scale_measure_timeout_label.grid( row=7, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.scale_connect_timeout_var = StringVar()
    self.scale_connect_timeout_label = Label(self.scale_status_frame, textvariable=self.scale_connect_timeout_var)
    self.scale_connect_timeout_label.grid( row=8, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.list_scale_measurements_button = Button(self.scale_status_frame, text="LIST MEASUREMENTS", width=30, height = 1, command = self.list_scale_measures_cmd)
    self.list_scale_measurements_button.grid(row=9, column = 1, padx=PADX, pady=PADX)
    

    self.app_actions_frame = LabelFrame(self.app_frame, text = "App actions")
    self.app_actions_frame.grid( row = 1, column = 1, padx=PADX, pady=PADX)

    self.bluetooth_var = IntVar()
    self.bluetooth_checkbutton = Checkbutton(self.app_actions_frame, text="BLUETOOTH ON", width=30, height = 1, command = self.bluetooth_cmd, variable = self.bluetooth_var)
    self.bluetooth_checkbutton.grid(row=1, column = 1, padx=PADX, pady=PADX)
    
    self.app_pair_button = Button(self.app_actions_frame, text="ACCEPT BONDING", width=30, height = 1, command = self.accept_pairing_cmd)
    self.app_pair_button.grid(row=2, column = 1, padx=PADX, pady=PADX)
    
    self.app_unpair_button = Button(self.app_actions_frame, text="REMOVE BONDING", width=30, height = 1, command = self.remove_pairing_cmd)
    self.app_unpair_button.grid(row=3, column = 1, padx=PADX, pady=PADX)
    
    self.app_status_frame = LabelFrame(self.app_frame, text = "App status")
    self.app_status_frame.grid( row = 2, column = 1, padx=PADX, pady=PADX)
 
    self.app_paired_var = StringVar()
    self.app_paired_label = Label(self.app_status_frame, textvariable=self.app_paired_var)
    self.app_paired_label.grid( row=1, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.app_bonded_var = StringVar()
    self.app_bonded_label = Label(self.app_status_frame, textvariable=self.app_bonded_var)
    self.app_bonded_label.grid( row=2, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.app_advertised_var = StringVar()
    self.app_advertised_label = Label(self.app_status_frame, textvariable=self.app_advertised_var)
    self.app_advertised_label.grid( row=3, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.app_connected_var = StringVar()
    self.app_connected_label = Label(self.app_status_frame, textvariable=self.app_connected_var)
    self.app_connected_label.grid( row=4, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.app_measurements_var = StringVar()
    self.app_measurements_label = Label(self.app_status_frame, textvariable=self.app_measurements_var)
    self.app_measurements_label.grid( row=5, column=1, padx=PADX, pady=PADX, columnspan=1)
    
    self.list_measurements_button = Button(self.app_status_frame, text="LIST MEASUREMENTS", width=30, height = 1, command = self.list_app_measures_cmd)
    self.list_measurements_button.grid(row=6, column = 1, padx=PADX, pady=PADX)
    
    self.simulate()
    
  def list_app_measures_cmd(self):
    index = 1
    for measument in self.scaleapp.measurements:
      logger.info(("APP MEASUREMENT %d--->" % index) + str(measument))
      index = index + 1
      
  def list_scale_measures_cmd(self):
    index = 1
    for measument in self.scale.measurements:
      logger.info(("SCALE MEASUREMENT %d--->" % index) + str(measument))
      index = index + 1
      
  def update_app_status(self):
    self.app_paired_var.set("PAIRED = %d" % (self.scaleapp.paired_scale is not None ))
    self.app_bonded_var.set("BONDED = %d" % (self.scaleapp.bonded_scale is not None ))
    self.app_advertised_var.set("ADVERTISED = %d" % (self.scaleapp.advertised_scale is not None ))
    self.app_connected_var.set("CONNECTED = %d" % (self.scaleapp.connected))
    self.app_measurements_var.set("RECEIVED MEASUREMENTS COUNT = %d" % (len(self.scaleapp.measurements)))

  def update_scale_status(self):
    time_now = time.time()
    if self.scale.current_state == "power_off":
      self.scale_status_label.config(fg = "#800000")
    elif self.scale.current_state == "sleep":
      self.scale_status_label.config(fg = "#808000")
    else:
      self.scale_status_label.config(fg = "#008000")      
    self.scale_status_var.set("CURRENT STATE = %s" % self.scale.current_state)
#     if self.scale.sound != "0":
#       self.scale_sound_var.set("SOUND = %s" % self.scale.sound)
#     else:
#       self.scale_sound_var.set("")
    if len(self.scale.measurements):
      self.scale_measurements_var.set("STORED MEASUREMENTS COUNT = %d" % (len(self.scale.measurements)))
    else:
      self.scale_measurements_var.set("")
    if self.scale.current_measurement.weight:
      self.scale_current_measurement_var.set("CURRENT MEASUREMENT = %d kg" % self.scale.current_measurement.weight)
    else:
      self.scale_current_measurement_var.set("")
    if self.scale.current_notification:
      self.scale_notification_var.set("Notification %d" % self.scale.current_notification)
      self.scale_notification_label.config(fg = "#000080")
    else:
      self.scale_notification_var.set(" ")
      self.scale_notification_label.config(fg = "#000000")
    if self.scale.pairing_timeout:
      self.scale_pairing_timeout_var.set("PAIRING TIMEOUT = %d s" % ( time_now - self.scale.pairing_timeout))
    else:
      self.scale_pairing_timeout_var.set("")
    if self.scale.measure_timeout:
      self.scale_measure_timeout_var.set("MEASURE TIMEOUT = %d s" % ( time_now - self.scale.measure_timeout))
    else:
      self.scale_measure_timeout_var.set("")
    if self.scale.connect_timeout:
      self.scale_connect_timeout_var.set("CONNECT TIMEOUT = %d s" % ( time_now - self.scale.connect_timeout))
    else:
      self.scale_connect_timeout_var.set("")
    
  def accept_pairing_cmd(self):
    self.scaleapp.bond(True)
    
  def remove_pairing_cmd(self):
    self.scaleapp.bond(False)
    
  def bluetooth_cmd(self):
    self.scaleapp.bt_enable(self.bluetooth_var.get())
    
  def battery_cmd(self):
    self.scale.batteries = self.battery_var.get()
    if not self.scale.batteries:
      self.scale.bonding_mode = self.mode_var.get()
      self.scaleapp.bonding_mode = self.mode_var.get()
      self.scale.restart()
      self.scaleapp.restart()
    
  def pairing_cmd(self):
    self.scale.pairing_button = self.pairing_var.get()
  
  def step_cmd(self):
    self.scale.step_on = self.step_var.get()
    if self.step_var.get():
      self.scale.current_measurement = scale.scale.WeightMeasurement( self.weight_var.get() )
    else:
      self.scale.current_measurement = scale.scale.WeightMeasurement(0)
  
    
  def simulate(self):
    self.scale.run()
    self.update_scale_status()
    self.update_app_status()
    self.master.after(500, self.simulate)
    
if __name__ == '__main__':    
  logger.info("--------------------")
  logger.info("====== START =======")
  root = Tk()
  root.title("BELLABEAT SCALE SIMULATOR")
  my_scaleapp = scale.scaleapp.ScaleApp()
  my_scale = scale.scale.ScaleMockup( my_scaleapp )
  app = App(root, my_scale, my_scaleapp )
  root.mainloop()
